### Information

While adding node on OpenShift failed with following error:
  

### Symptom:


````python
printf "foo"
````


````javascript 
function javascriptIsWild(){ 
parseInt("Infinity", 10) // -> NaN 
}
```` 


```shellsession
failed: [10.168.119.1] (item=iptables-services) => {"attempts": 3, "changed": false, "item": "iptables-services", "msg": "No package matching 'iptables-services' found available, installed or updated", "rc": 126, "results": ["No package matching 'iptables-services' found available, installed or updated"]}
	to retry, use: --limit @/root/openshift/openshift-ansible/playbooks/openshift-node/scaleup.retry

PLAY RECAP *******************************************************************************************************************************************************************************************************
10.168.116.190             : ok=29   changed=0    unreachable=0    failed=0
10.168.119.1               : ok=35   changed=7    unreachable=0    failed=1
localhost                  : ok=22   changed=0    unreachable=0    failed=0


INSTALLER STATUS *************************************************************************************************************************************************************************************************
Initialization             : Complete (0:01:32)
```
  

### Origin:

OpenShift required firewalld service on Node.
  

### Solution:

Install firewalld package using yum on Node.


```shellsession
[root@node11 ~]# yum install -y iptables-services
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
....
....
....
[root@node11 ~]# systemctl status iptables
● iptables.service - IPv4 firewall with iptables
   Loaded: loaded (/usr/lib/systemd/system/iptables.service; enabled; vendor preset: disabled)
   Active: active (exited) since Tue 2019-02-26 04:14:38 EST; 1 day 20h ago
 Main PID: 759 (code=exited, status=0/SUCCESS)
    Tasks: 0
   Memory: 0B
   CGroup: /system.slice/iptables.service

Feb 26 04:14:34 node11 systemd[1]: Starting IPv4 firewall with iptables...
Feb 26 04:14:38 node11 iptables.init[759]: iptables: Applying firewall rules: [  OK  ]
Feb 26 04:14:38 node11 systemd[1]: Started IPv4 firewall with iptables.


```
> Written with [StackEdit](https://stackedit.io/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIxMDIzNTA5NjgsNzI1MDEzNjU0XX0=
-->