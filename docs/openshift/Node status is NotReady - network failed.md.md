
**SOS - Node status is NotReady
Error - 'runtime network not ready: NetworkReady=false reason:NetworkPluginNotReady**


### Symptoms

```shellsession
[root@node2 ~]# oc get nodes
NAME               STATUS     ROLES            AGE       VERSION
node2.sevone.com   NotReady   compute          14m       v1.9.1+a0ce1bc657
node3.sevone.com   Ready      compute          14m       v1.9.1+a0ce1bc657
openshift1         Ready      compute,master   10h       v1.9.1+a0ce1bc657
```

```shellsession
    - lastHeartbeatTime: 2019-02-28T03:02:48Z
    lastTransitionTime: 2019-02-28T02:35:58Z
    message: kubelet has no disk pressure
    reason: KubeletHasNoDiskPressure
    status: "False"
    type: DiskPressure
  - lastHeartbeatTime: 2019-02-28T03:02:48Z
    lastTransitionTime: 2019-02-28T02:35:58Z
    message: 'runtime network not ready: NetworkReady=false reason:NetworkPluginNotReady
```

### Origin

The ansible installer sometimes fails to create the file /etc/cni/net.d/80-openshift-network.conf which causes the node to fail. 

Expected Results
All nodes should start. The file /etc/cni/net.d/80-openshift-network.conf should be present on all nodes and the origin-node service should start successfully.

Observed Results
On the failed nodes the file /etc/cni/net.d/80-openshift-network.conf is not present and the origin-node service fails to start.

```shellsession
[root@node2 ~]# sudo systemctl status -l origin-node.service
● origin-node.service
   Loaded: loaded (/etc/systemd/system/origin-node.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2019-02-27 22:10:30 EST; 1min 19s ago
  Process: 21656 ExecStopPost=/usr/bin/dbus-send --system --dest=uk.org.thekelleys.dnsmasq /uk/org/thekelleys/dnsmasq uk.org.thekelleys.SetDomainServers array:string: (code=exited, status=0/SUCCESS)
  Process: 21654 ExecStopPost=/usr/bin/rm /etc/dnsmasq.d/node-dnsmasq.conf (code=exited, status=0/SUCCESS)
  Process: 21649 ExecStop=/usr/bin/docker stop origin-node (code=exited, status=1/FAILURE)
  Process: 21670 ExecStartPost=/usr/bin/sleep 10 (code=exited, status=0/SUCCESS)
  Process: 21667 ExecStartPre=/usr/bin/dbus-send --system --dest=uk.org.thekelleys.dnsmasq /uk/org/thekelleys/dnsmasq uk.org.thekelleys.SetDomainServers array:string:/in-addr.arpa/127.0.0.1,/cluster.local/127.0.0.1 (code=exited, status=0/SUCCESS)
  Process: 21665 ExecStartPre=/usr/bin/cp /etc/origin/node/node-dnsmasq.conf /etc/dnsmasq.d/ (code=exited, status=0/SUCCESS)
  Process: 21660 ExecStartPre=/usr/bin/docker rm -f origin-node (code=exited, status=1/FAILURE)
 Main PID: 21669 (docker-current)
    Tasks: 4
   Memory: 4.7M
   CGroup: /system.slice/origin-node.service
           └─21669 /usr/bin/docker-current run --name origin-node --rm --privileged --net=host --pid=host --env-file=/etc/sysconfig/origin-node -v /:/rootfs:ro,rslave -e CONFIG_FILE=/etc/origin/node/node-config.yaml -e OPTIONS=--loglevel=2 -e HOST=/rootfs -e HOST_ETC=/host-etc -v /var/lib/origin:/var/lib/origin:rslave -v /etc/origin/kubelet-plugins:/etc/origin/kubelet-plugins:rslave -v /etc/origin/node:/etc/origin/node -v /etc/localtime:/etc/localtime:ro -v /etc/machine-id:/etc/machine-id:ro -v /run:/run -v /sys:/sys:rw -v /sys/fs/cgroup:/sys/fs/cgroup:rw -v /usr/bin/docker:/usr/bin/docker:ro -v /var/lib/docker:/var/lib/docker -v /lib/modules:/lib/modules -v /etc/origin/openvswitch:/etc/openvswitch -v /etc/origin/sdn:/etc/openshift-sdn -v /var/lib/cni:/var/lib/cni -v /etc/systemd/system:/host-etc/systemd/system -v /var/log:/var/log -v /dev:/dev --volume=/usr/bin/docker-current:/usr/bin/docker-current:ro --volume=/etc/sysconfig/docker:/etc/sysconfig/docker:ro --volume=/etc/containers/registries:/etc/containers/registries:ro -v /etc/pki:/etc/pki:ro -v /var/lib/kubelet:/var/lib/kubelet openshift/node:v3.9.0

Feb 27 22:11:40 node2.lab.example.com origin-node[21669]: E0227 22:11:40.024169   21689 kubelet.go:2106] Container runtime network not ready: NetworkReady=false reason:NetworkPluginNotReady message:docker: network plugin is not ready: cni config uninitialized
Feb 27 22:11:41 node2.lab.example.com origin-node[21669]: E0227 22:11:41.925043   21689 kubelet_volumes.go:128] Orphaned pod "126e27ee-3a81-11e9-8beb-0050568c7c22" found, but volume paths are still present on disk : There were a total of 4 errors similar to this. Turn up verbosity to see them.
Feb 27 22:11:43 node2.lab.example.com origin-node[21669]: E0227 22:11:43.917492   21689 kubelet_volumes.go:128] Orphaned pod "126e27ee-3a81-11e9-8beb-0050568c7c22" found, but volume paths are still present on disk : There were a total of 4 errors similar to this. Turn up verbosity to see them.
Feb 27 22:11:45 node2.lab.example.com origin-node[21669]: W0227 22:11:45.026114   21689 cni.go:171] Unable to update cni config: No networks found in /etc/cni/net.d
Feb 27 22:11:45 node2.lab.example.com origin-node[21669]: E0227 22:11:45.026296   21689 kubelet.go:2106] Container runtime network not ready: NetworkReady=false reason:NetworkPluginNotReady message:docker: network plugin is not ready: cni config uninitialized
Feb 27 22:11:45 node2.lab.example.com origin-node[21669]: E0227 22:11:45.920710   21689 kubelet_volumes.go:128] Orphaned pod "126e27ee-3a81-11e9-8beb-0050568c7c22" found, but volume paths are still present on disk : There were a total of 4 errors similar to this. Turn up verbosity to see them.
Feb 27 22:11:47 node2.lab.example.com origin-node[21669]: E0227 22:11:47.918674   21689 kubelet_volumes.go:128] Orphaned pod "126e27ee-3a81-11e9-8beb-0050568c7c22" found, but volume paths are still present on disk : There were a total of 4 errors similar to this. Turn up verbosity to see them.
Feb 27 22:11:49 node2.lab.example.com origin-node[21669]: E0227 22:11:49.920680   21689 kubelet_volumes.go:128] Orphaned pod "126e27ee-3a81-11e9-8beb-0050568c7c22" found, but volume paths are still present on disk : There were a total of 4 errors similar to this. Turn up verbosity to see them.
Feb 27 22:11:50 node2.lab.example.com origin-node[21669]: W0227 22:11:50.028145   21689 cni.go:171] Unable to update cni config: No networks found in /etc/cni/net.d
Feb 27 22:11:50 node2.lab.example.com origin-node[21669]: E0227 22:11:50.028316   21689 kubelet.go:2106] Container runtime network not ready: NetworkReady=false reason:NetworkPluginNotReady message:docker: network plugin is not ready: cni config uninitialized
```

### Solution


```shellsession
[root@node2 ~]# cat /etc/cni/net.d/80-openshift-network.conf
cat: /etc/cni/net.d/80-openshift-network.conf: No such file or directory
[root@node2 ~]# cat <<EOF > /etc/cni/net.d/80-openshift-network.conf
>
> {
>   "cniVersion": "0.2.0",
>   "name": "openshift-sdn",
>   "type": "openshift-sdn"
> }
> EOF

[root@node2 ~]# reboot
Connection to 10.168.119.2 closed by remote host.
Connection to 10.168.119.2 closed.

[root@node2 ~]# systemctl status origin-node.service -l
● origin-node.service
   Loaded: loaded (/etc/systemd/system/origin-node.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2019-02-27 22:47:45 EST; 49s ago
  Process: 2470 ExecStartPost=/usr/bin/sleep 10 (code=exited, status=0/SUCCESS)
....
....
....
[root@node2 ~]# oc get nodes
NAME               STATUS    ROLES            AGE       VERSION
node2.sevone.com   Ready     compute          2h        v1.9.1+a0ce1bc657
node3.sevone.com   Ready     compute          2h        v1.9.1+a0ce1bc657
openshift1         Ready     compute,master   13h       v1.9.1+a0ce1bc657
```

Source: https://github.com/openshift/openshift-ansible/issues/7967

> Written with [StackEdit](https://stackedit.io/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTMyNzQ4MDgzNV19
-->