### Information

Node addition failed with following error message.

### Symptom:

```shellsession
TASK [Validate openshift_hostname when defined] ******************************************************************************************************************************************************************
fatal: [10.168.119.4]: FAILED! => {"msg": "The task includes an option with an undefined variable. The error was: 'openshift_hostname' is undefined\n\nThe error appears to have been in '/root/installcentos/openshift-ansible/playbooks/init/validate_hostnames.yml': line 13, column 5, but may\nbe elsewhere in the file depending on the exact syntax problem.\n\nThe offending line appears to be:\n\n\n  - name: Validate openshift_hostname when defined\n    ^ here\n"}

NO MORE HOSTS LEFT ***********************************************************************************************************************************************************************************************
	to retry, use: --limit @/root/installcentos/openshift-ansible/playbooks/openshift-node/scaleup.retry

PLAY RECAP *******************************************************************************************************************************************************************************************************
10.168.117.1               : ok=29   changed=0    unreachable=0    failed=0
10.168.119.4               : ok=20   changed=1    unreachable=0    failed=1
localhost                  : ok=22   changed=0    unreachable=0    failed=0


INSTALLER STATUS *************************************************************************************************************************************************************************************************
Initialization             : Complete (0:01:08)

```

### Origin:

The error message would indicate that, openshift_hostname is undefined. i.e. This varialbe is expcted in invenotry file.


### Solution:

Added **openshift_hostname** variable into inventory.ini and rerun the scaleup.yaml

```shellsession
[new_nodes]
10.168.119.4 openshift_ip=10.168.119.4 openshift_hostname=node4.sevone.com openshift_schedulable=true openshift_node_labels="{'region': 'primary', 'zone': 'west'}"

```
