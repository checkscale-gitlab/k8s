---
Title:  Code Demo
Author: Amit Karpe
Date:   November 15, 2018
heroimage: "http://i.imgur.com/rBX9z0k.png"
tags:
- linux
- open source
hero: Various Note options samples
path: blob/master/docs
source: note.md

---

[TOC]

### snippets


--8<-- "new.md" 


GitHub:

??? snippet "yaml code - Open Collapsible blocks"
    <script src="https://gist.github.com/amitkarpe/42c2d7b97b1abafd87eac6f3d98bad9b.js"></script>

GitLab:

??? snippet "yaml code - Open Collapsible blocks"
    <script src="https://gitlab.com/amitkarpe/awx/snippets/1760618.js"></script>




<details>
<summary>GitHub</summary>
<script src="https://gist.github.com/amitkarpe/42c2d7b97b1abafd87eac6f3d98bad9b.js"></script>
</details>


---

### Code with tabs


``` python hl_lines="3 4" tab=""
""" Bubble sort """
def bubble_sort(items):
    for i in range(len(items)):
        for j in range(len(items) - 1 - i):
            if items[j] > items[j + 1]:
                items[j], items[j + 1] = items[j + 1], items[j]
```

``` bash tab="Bash"
#!/bin/bash

echo "Hello world!"
```

``` c tab="C"
#include <stdio.h>

int main(void) {
  printf("Hello world!\n");
}
```

``` c++ tab="C++"
#include <iostream>

int main() {
  std::cout << "Hello world!" << std::endl;
  return 0;
}
```

``` c# tab="C#"
using System;

class Program {
  static void Main(string[] args) {
    Console.WriteLine("Hello world!");
  }
}
```



## Admonition


???+ snippet "yaml code - Open Collapsible blocks"
    ```yaml
    #!/usr/bin/ansible-playbook
    # --- Install snmpd package
    - hosts: '{{ host }}'
    gather_facts: false
    user: '{{ user }}'
    tasks:
    - name: Install snmpd package
        apt: name={{item}} state=present
    ```

???+ snippet "Python Code - Open Collapsible blocks"
    ```python
    import tensorflow as tf
    ```

??? snippet "PHP Code - Closed Collapsible blocks"
    ```php
    <!DOCTYPE html>
    <html>
    <body>

    <?php
    class Car {
        function Car() {
            $this->model = "VW";
        }
    }
    // create an object
    $herbie = new Car();

    // show object properties
    echo $herbie->model;
    ?>

    </body>
    </html>
    ```


---

### Nested blocks


- [ ] unchecked
- [x] checked

Markdown | Less | Pretty
--- | :---: | ---
*Still* | `renders` | **nicely**
1 | 2 | 3
 zebra stripes | are neat      |    $1 
a | b | c
r | t | y


???+ summary

    This is the **note** admonition body

    !!! danger Danger Title
        This is the **danger** admonition body


!!! note

    This is the **note** admonition body

    ??? danger Danger Title
        This is the **danger** admonition body


---

### Collapsible blocks


??? note "Collapsible blocks"

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

---

### Embeded blocks

!!! note "Embeded blocks"

    This is mysql code

    ``` mysql
    SELECT
      Employees.EmployeeID,
      Employees.Name,
      Employees.Salary,
      Manager.Name AS Manager
    FROM
      Employees
    LEFT JOIN
      Employees AS Manager
    ON
      Employees.ManagerID = Manager.EmployeeID
    WHERE
      Employees.EmployeeID = '087652';
    ```

---

### code without lang



```shell
#!/bin/bash

for OPT in "$@"
do
  case "$OPT" in
    '-f' )  canonicalize=1 ;;
    '-n' )  switchlf="-n" ;;
  esac
done
exit $?
```

