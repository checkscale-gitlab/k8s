---
Title:  Code Demo
Author: Amit Karpe
Date:   November 15, 2018
heroimage: "https://monosnap.com/file/p8w75bxxZ0nwPt4qMsxfd2mppRsjxL"
heroimage: "https://monosnap.com/image/p8w75bxxZ0nwPt4qMsxfd2mppRsjxL.png"
tags:
- linux
- open source
hero: Various code samples
path: blob/master/docs
source: code.md

---


[TOC]

## Header 1

<details>
<summary>Click to expand</summary>
this is hidden
</details>


## Image demo
Inline-style: 
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

Reference-style: 
![alt text][logo]
[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"
<!-- .element style="border: 2; background: None; box-shadow: None" -->


### [emoji](https://facelessuser.github.io/pymdown-extensions/extensions/emoji/)

:smile: :angry: :smile: :heart: :thumbsup:



### Video


<!---
<iframe width="260" src="https://www.youtube.com/embed/OtqFyBA6Dbk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<video src="https://sec.ch9.ms/ch9/7247/7c8ddc1a-348b-4ba9-ab61-51fded6e7247/vstswiki_high.mp4" width=400 controls>
</video>


-->




#### More images

<!---
Image with 250 width
![very good|500,20%](https://monosnap.com/image/p8w75bxxZ0nwPt4qMsxfd2mppRsjxL.png )
![demo](https://monosnap.com/image/p8w75bxxZ0nwPt4qMsxfd2mppRsjxL.png =100x20)
Image with 20X20
<img src="https://monosnap.com/image/p8w75bxxZ0nwPt4qMsxfd2mppRsjxL.png" alt="Sparsh Karpe" width="20" height="20" />
-->

Image with 200 width

<img src="https://monosnap.com/image/p8w75bxxZ0nwPt4qMsxfd2mppRsjxL.png" alt="Sparsh Karpe" width="200"/>
<!-- .element style="border: 2; background: None; box-shadow: None" -->



## Header 2

[//]: # "Comment - ![Alt text](https://monosnap.com/image/p8w75bxxZ0nwPt4qMsxfd2mppRsjxL.png)"


``` curl tab="Using curl"
$ curl -O wget http://example.com/pk.zip
```

``` wget tab="Using wget"
$ wget http://example.com/pk.zip
```

``` c tab="This is *C* program example"
#include <stdio.h>

int main(void) {
  printf("Hello world!\n");
}
```

``` bash tab="Bash"
#!/bin/bash

echo "Hello world!"
```

``` c tab="C"
#include <stdio.h>

int main(void) {
  printf("Hello world!\n");
}
```


## Header 3


```c tab="                           Demo for c"
#include <stdio.h>

int main(void) {
  printf("Hello world!\n");
}

```